window.onload = function() {
  document.querySelector(`.tabs_header`).addEventListener(`click`, fTabs);
  function fTabs(event) {
    if (event.target.className == `tab_h flex_centring`) {
      const dataTab = event.target.getAttribute(`data-tab`);
      const tabH = document.getElementsByClassName(`tab_h`);
      for (let i = 0; i < tabH.length; i++) {
        tabH[i].classList.remove(`active`);
      }
      event.target.classList.add(`active`);
      const tabBody = document.getElementsByClassName(`tab_b`);
      for (let i = 0; i < tabBody.length; i++) {
        if (dataTab == i) {
          tabBody[i].style.display = `block`;
        } else {
          tabBody[i].style.display = `none`;
        }
      }
    }
  }
};

const OurWorkFilterBtns = document.querySelectorAll(`.our_works_tab_h`);
console.log(OurWorkFilterBtns);

const ourWorksAddBtn = document.querySelector(`.our_works_btn`);

let counter = 0;
const ourWorksPhoto = document.querySelector(`.our_works_photo`);
function genereitDataTab(i) {
  if (i <= 3) {
    return 1;
  }
  if (i <= 6) {
    return 2;
  }
  if (i <= 9) {
    return 3;
  }
  if (i <= 12) {
    return 4;
  }
}

function FilterByDataTab(arr, dataTab) {
  arr.forEach(function(k) {
    if (dataTab !== k.getAttribute(`data-tab`)) {
      k.style.display = `none`;
    } else {
      k.style.display = `flex`;
    }
    if (dataTab === `0`) {
      k.style.display = `flex`;
    }
  });
}

function load12Pictures() {
  counter++;
  for (let i = 0; i < 12; i++) {
    const block = `<div class="flex_centring our_works_bottom" data-tab="${genereitDataTab(
      i + 1
    )}"><img src="https://picsum.photos/286/211/?image=${counter *
      (i + 20)}" alt="some photo"></div>`;
    ourWorksPhoto.innerHTML += block;
  }
  if (counter >= 3) {
    ourWorksAddBtn.style.display = `none`;
  }

  const dataTab = document
    .querySelector(`.our_works_tab_h-active`)
    .getAttribute(`data-tab`);
  const imgArr = document.querySelectorAll('.our_works_bottom');
  FilterByDataTab(imgArr, dataTab);
}
load12Pictures();

function ourWorksFilter(event) {
  document
    .querySelector('.our_works_tab_h-active')
    .classList.remove(`our_works_tab_h-active`);

  event.target.classList.add('our_works_tab_h-active');

  const dataTab = event.target.getAttribute(`data-tab`);
  const imgArrey = document.querySelectorAll(`.our_works_bottom`);
  FilterByDataTab(imgArrey, dataTab);
}

OurWorkFilterBtns.forEach(function(i) {
  i.addEventListener(`click`, ourWorksFilter);
});

ourWorksAddBtn.addEventListener(`click`, load12Pictures);

const blogPhotoAll = document.querySelector(`.blog_photo_all`);
function load8Pictures() {
  for (let i = 0; i < 8; i++) {
    const block = `<div class="blog_photo_one column">
                        <div class="square_for_data flex_centring">
                            <div class="data_in_square flex_centring column"><span>12</span> <span>Feb</span></div>
                        </div>
                        <div class="breaking_news_photo flex_centring column"><a class="flex_centring" href="#"><img src="https://picsum.photos/263/208/?image=${counter *
                          (i + 25)}" alt="some photo"></a></div>
                        <div class="first_text_after_photo ">Amazing Image Post</div>
                        <div class="second_text_after_photo"><span>By admin</span> | <span>2 comment</span></div>
                    </div>`;
    blogPhotoAll.innerHTML += block;
  }
}
load8Pictures();

const carouselFooterBtnLeft = document.querySelector(
  `.carousel_footer_btn_left`
);
const carouselFooterBtnRight = document.querySelector(
  `.carousel_footer_btn_right`
);
const carouselDataTab = document.querySelectorAll(`.carousel_footer_img`);
let carouselCounter = 0;
const carouselCounterControl = direction => {
  carouselCounter =
    direction === 'prev'
      ? carouselCounter - 1
      : direction === 'next'
        ? carouselCounter + 1
        : direction;
  carouselCounter =
    carouselCounter > 3 ? 0 : carouselCounter < 0 ? 3 : carouselCounter;
};
const dataTabActive = () => {
  const nowActiveDataTAb = document.querySelector(`.data_tab_active`);
  nowActiveDataTAb.classList.remove(`data_tab_active`);
  carouselDataTab.forEach(item => {
    +item.getAttribute(`data-tab`) === carouselCounter
      ? item.classList.add(`data_tab_active`)
      : undefined;
  });
};

const getViewportSize = () => {
  const viewport = document.querySelector(`.carousel_body`);
  const { width } = window.getComputedStyle(viewport);
  return parseInt(width);
};

const carouselBodyItem = document.querySelectorAll(`.carousel_body_container_item`);
const changeOpacity = () => {
  const nowChangeOpacity = document.querySelector(
    `.carousel_body_container_item_active`
  );
  nowChangeOpacity.classList.remove(`carousel_body_container_item_active`);

    carouselBodyItem.forEach(item => {
    +item.getAttribute(`data-tab`) === carouselCounter
      ? item.classList.add(`carousel_body_container_item_active`) : undefined;
  });
};

const rotateSlider = width => {
  const container = document.querySelector(`.carousel_body_container`);
  container.style.transform = `translateX(${-carouselCounter * width}px)`;
    changeOpacity();
};

const slideControlsHandler = direction => {
  carouselCounterControl(direction);
  dataTabActive();
  const width = getViewportSize();
  rotateSlider(width);
};

carouselFooterBtnLeft.addEventListener(`click`, () =>
  slideControlsHandler('prev')
);
carouselFooterBtnRight.addEventListener(`click`, () =>
  slideControlsHandler('next')
);
carouselDataTab.forEach(item =>
  item.addEventListener(`click`, e =>
    slideControlsHandler(+e.currentTarget.getAttribute('data-tab'))
  )
);

// masonry

// vanilla JS
// init with element
let grid = document.querySelector('.grid');
let msnry = new Masonry( grid, {
    // options...
    itemSelector: '.grid-item',
    columnWidth: 200
});

// init with selector
// let msnry = new Masonry( '.grid', {
//     // options...
// });